(function() {

    'use strict';

    angular.module('appAsDeusas.services', []);
    angular.module('appAsDeusas.directives', []);
    angular.module('appAsDeusas.controllers', []);

    angular.module('appAsDeusas', ['ionic', 'appAsDeusas.httpInterceptor', 'appAsDeusas.services', 'appAsDeusas.directives', 'appAsDeusas.controllers'])

    .run(function($ionicPlatform) {
        $ionicPlatform.ready(function() {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });
    })

    .config(function($stateProvider, $urlRouterProvider, $httpProvider) {

        $stateProvider
            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'menuCtrl as vm'
            })
            .state('app.home', {
                url: '/home',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/home.html',
                        controller: 'homeCtrl as vm'
                    }
                }
            })
            .state('app.sobre', {
                url: '/sobre',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/sobre.html',
                        controller: 'sobreCtrl as vm'
                    }
                }
            });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/home');
        // Add the interceptor to the $httpProvider.
        $httpProvider.interceptors.push('httpInterceptor');
    });
})();