(function() {

	'use strict';

	/* jshint validthis: true */

	angular.module('appAsDeusas.services')
		.factory('$api', api);

	api.$inject = ['$http'];

	function api($http) {

		var urlBase = 'http://www.criarleiloes.com.br/app/',
			api = {
				getSobre  : getSobre,
				getAgenda : getAgenda,
				getLotes  : getLotes,
				sendEmail : sendEmail
			};

		return api;

		///////////////////////////////////////


		function getSobre() {
			return $http.get( urlBase + 'sobre.php' );
		}

		function getAgenda() {
			return $http.get( urlBase + 'agenda.php' );
		}

		function getLotes(agendaID) {
			return $http.get( urlBase + 'lotes.php', { params: { id: agendaID }} );
		}

		function sendEmail(params) {
			return $http({ headers: {'Content-Type': 'application/x-www-form-urlencoded'}, url: urlBase + 'email.php', method: "POST", data: params });
		}
	}

})();